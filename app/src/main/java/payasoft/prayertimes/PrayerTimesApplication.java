package payasoft.prayertimes;

import android.app.Application;

import net.time4j.android.ApplicationStarter;

/**
 * Created by Reza on 2017/10/31.
 */

public class PrayerTimesApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationStarter.initialize(this, true); // with prefetch on background thread
    }
}
