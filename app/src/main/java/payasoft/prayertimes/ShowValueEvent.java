package payasoft.prayertimes;

/**
 * Created by Reza on 2017/11/01.
 */

public interface ShowValueEvent {
    public void show();
}
