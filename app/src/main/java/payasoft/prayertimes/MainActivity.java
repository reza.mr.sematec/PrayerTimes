package payasoft.prayertimes;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;

import net.time4j.PlainDate;
import net.time4j.calendar.PersianCalendar;
import net.time4j.format.expert.ChronoFormatter;

import java.util.Arrays;
import java.util.Locale;

import payasoft.prayertimes.Fragments.PrayerTimesFragment;
import payasoft.prayertimes.adapters.CityPagerAdapter;
import payasoft.prayertimes.adapters.PrayerTimeListAdapter;

public class MainActivity extends AppCompatActivity {
    String names[];
    String titles[];
    ViewPager cityPager;
    CityPagerAdapter cityPagerAdapter;
    PagerSlidingTabStrip tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        names = getResources().getStringArray(R.array.city_names);
        titles = getResources().getStringArray(R.array.city_titles);
        bindView();

        if (Locale.getDefault().getLanguage().equalsIgnoreCase("fa"))
            cityPager.setRotationY(180);
        cityPagerAdapter = new CityPagerAdapter(getSupportFragmentManager(), this, names, titles);
        cityPager.setAdapter(cityPagerAdapter);
        tabs.setViewPager(cityPager);

        PlainDate today = PlainDate.nowInSystemTime();
        ((TextView) findViewById(R.id.dateTextView)).setText(getDateTitle(today));
    }

    private void bindView() {
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        cityPager = (ViewPager) findViewById(R.id.cityPager);
    }

    public String getDateTitle(PlainDate gregorianDate) {
        String res;
        ChronoFormatter f;

        if (Locale.getDefault().getCountry() == "IR") {
            PersianCalendar jalali = gregorianDate.transform(PersianCalendar.class);
            f = ChronoFormatter.setUp(PersianCalendar.axis(), Locale.getDefault())
                    .addText(PersianCalendar.DAY_OF_WEEK)
                    .addLiteral('،')
                    .addLiteral(' ')
                    .addInteger(PersianCalendar.DAY_OF_MONTH, 1, 2)
                    .addLiteral(' ')
                    .addText(PersianCalendar.MONTH_OF_YEAR)
                    .addLiteral(' ')
                    .addInteger(PersianCalendar.YEAR_OF_ERA, 1, 4)
                    .build();
            res = f.format(jalali);
        } else {
            f = ChronoFormatter.setUp(PlainDate.axis(), Locale.getDefault())
                    .addText(PlainDate.DAY_OF_WEEK)
                    .addLiteral(',')
                    .addLiteral(' ')
                    .addInteger(PlainDate.DAY_OF_MONTH, 1, 2)
                    .addLiteral(' ')
                    .addText(PlainDate.MONTH_OF_YEAR)
                    .addLiteral(' ')
                    .addInteger(PlainDate.YEAR, 1, 4)
                    .build();
            res = f.format(gregorianDate);
        }

        return res;
    }

}
