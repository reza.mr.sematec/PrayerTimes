package payasoft.prayertimes.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Locale;

import payasoft.prayertimes.Fragments.PrayerTimes4GPSFragment;
import payasoft.prayertimes.Fragments.PrayerTimesFragment;
import payasoft.prayertimes.R;

/**
 * Created by Reza on 2017/11/17.
 */

public class CityPagerAdapter extends FragmentPagerAdapter {
    Context mContext;
    String names[];
    String titles[];
    PrayerTimesFragment fList[];

    public CityPagerAdapter(FragmentManager fm, Context mContext, String names[], String titles[]) {
        super(fm);
        this.mContext = mContext;
        this.names = names;
        this.titles = titles;
        fList = new PrayerTimesFragment[names.length];
        for (int i = 0; i < fList.length; i++) {
            fList[i] = new PrayerTimesFragment();
        }
    }

    @Override
    public Fragment getItem(int position) {
        if (position < names.length) {
            String city = names[position];
            if (city.equalsIgnoreCase("GPS")) {
                return PrayerTimes4GPSFragment.getInstance();
            } else {
                PrayerTimesFragment f;
                f = fList[position];
                f.setCity(city);
                return f;
            }
        } else return null;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position < titles.length)
            return titles[position];
        else return null;
    }

}
