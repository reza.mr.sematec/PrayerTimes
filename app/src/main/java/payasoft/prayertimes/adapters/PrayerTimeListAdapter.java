package payasoft.prayertimes.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import payasoft.prayertimes.Fragments.PrayerTimesFragment;
import payasoft.prayertimes.R;

/**
 * Created by Reza on 2017/10/30.
 */

public class PrayerTimeListAdapter extends BaseAdapter {
    Drawable icons[];
    String names[];
    String titles[];
    String values[];
    Context mContext;

    public void setValues(String[] values) {
        this.values = values;
    }

    public PrayerTimeListAdapter(Drawable[] icons, String[] names, String[] titles, String[] values, Context mContext) {
        this.icons = icons;
        this.names = names;
        this.titles = titles;
        this.values = values;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public Object getItem(int i) {
        return titles[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.prayer_time_item, viewGroup, false);

        ImageView icon = (ImageView) rowView.findViewById(R.id.timeIcon);
        icon.setImageDrawable(icons[i]);

        TextView title = (TextView) rowView.findViewById(R.id.timeTitle);
        title.setText(titles[i]);

        TextView value = (TextView) rowView.findViewById(R.id.timeValue);
        if (values != null && i<values.length && values[i] != null)
            value.setText(values[i]);
        else value.setText("");

        return rowView;
    }
}
