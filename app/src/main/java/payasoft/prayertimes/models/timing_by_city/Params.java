
package payasoft.prayertimes.models.timing_by_city;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Params {

    @SerializedName("Fajr")
    @Expose
    private Double fajr;
    @SerializedName("Isha")
    @Expose
    private Integer isha;
    @SerializedName("Maghrib")
    @Expose
    private Double maghrib;
    @SerializedName("Midnight")
    @Expose
    private String midnight;

    public Double getFajr() {
        return fajr;
    }

    public void setFajr(Double fajr) {
        this.fajr = fajr;
    }

    public Integer getIsha() {
        return isha;
    }

    public void setIsha(Integer isha) {
        this.isha = isha;
    }

    public Double getMaghrib() {
        return maghrib;
    }

    public void setMaghrib(Double maghrib) {
        this.maghrib = maghrib;
    }

    public String getMidnight() {
        return midnight;
    }

    public void setMidnight(String midnight) {
        this.midnight = midnight;
    }

}
