package payasoft.prayertimes.Fragments;


import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.time4j.PlainDate;
import net.time4j.calendar.PersianCalendar;
import net.time4j.format.expert.ChronoFormatter;

import java.util.Arrays;
import java.util.Locale;

import payasoft.prayertimes.PrayerTime;
import payasoft.prayertimes.PrayerTimingDBHelper;
import payasoft.prayertimes.R;
import payasoft.prayertimes.ShowValueEvent;
import payasoft.prayertimes.adapters.PrayerTimeListAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrayerTimesFragment extends Fragment implements View.OnClickListener {

    private Drawable icons[];
    private String names[];  // match with API names
    private String titles[]; // for show n UI
    private String values[];
    private ListView timeListView;
    private boolean refreshed;
    private View v;
    private String city = "";

    public void setFlip(boolean flip) {
        this.flip = flip;
        if (Locale.getDefault().getLanguage().equalsIgnoreCase("fa")) {
         if (flip)
            v.setRotationY(180);
         else v.setRotationY(0);
        }
    }

    private boolean flip = true;

    public void setLoc(Location loc) {
        this.loc = loc;
    }

    private Location loc;

    PrayerTimeListAdapter prayerTimeListAdapter;
    PrayerTime prayerTime;
    PrayerTimingDBHelper dbHandler;

    public String getCity() {
        return city;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        refreshValue();
    }

    public void setCity(String city) {
        if (this.city != city) {
            this.city = city;
//            refreshValue();
        }
    }

    public PrayerTimesFragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_prayer_times, container, false);
        // Get icons from resource
        TypedArray ta = getResources().obtainTypedArray(R.array.prayer_time_icons);
        icons = new Drawable[ta.length()];
        try {
            for (int i = 0; i < ta.length(); i++)
                icons[i] = ta.getDrawable(i);
        } catch (Exception e) {
            Log.d("c", e.toString());
        }

        // Get titles from resources
        titles = getResources().getStringArray(R.array.prayer_time_titles);
        // Get names from resources
        names = getResources().getStringArray(R.array.prayer_time_names);

        // View Pager RTL support
        if (Locale.getDefault().getLanguage().equalsIgnoreCase("fa"))
            v.setRotationY(180);

        prayerTimeListAdapter = new PrayerTimeListAdapter(icons, names, titles, values, getActivity());
        dbHandler = new PrayerTimingDBHelper(getActivity(), "PrayerTiming.db", null, 1);
        bindViews();
        refreshValue();

        // Inflate the layout for this fragment
        return v;
    }


    private void bindViews() {
        timeListView = (ListView) v.findViewById(R.id.timeListView);
        timeListView.setAdapter(prayerTimeListAdapter);

//        v.findViewById(R.id.refreshButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        refreshValue();
    }

    public void refreshValue() {
        ShowValueEvent showValueEvent = new ShowValueEvent() {
            @Override
            public void show() {
                values = new String[names.length];
                for (int i = 0; i < names.length; i++) {
                    values[i] = prayerTime.getAPrayerTime(names[i]);
                }
                ;
                prayerTimeListAdapter.setValues(values);
                prayerTimeListAdapter.notifyDataSetChanged();
            }
        };

        PlainDate today = PlainDate.nowInSystemTime();

//        prayerTime = new PrayerTime(getActivity(), city, dbHandler, new ShowValueEvent() {
//            @Override
//            public void show() {
//                values = new String[names.length];
//                for (int i = 0; i < names.length; i++) {
//                    values[i] = prayerTime.getAPrayerTime(names[i]);
//                }
//                ;
//                prayerTimeListAdapter.setValues(values);
//                prayerTimeListAdapter.notifyDataSetChanged();
//            }
//        });
        if (loc != null)
            prayerTime = new PrayerTime(getActivity(), loc, showValueEvent);
        else prayerTime = new PrayerTime(getActivity(), city, dbHandler, showValueEvent);
        if (city != "" || loc != null)  // Initial state
            prayerTime.refresh();
    }

    public String getCityTitle(String city) {
        String citiesName[] = getResources().getStringArray(R.array.city_names);
        String citiesTitle[] = getResources().getStringArray(R.array.city_titles);

        int i = Arrays.asList(citiesName).indexOf(city);
        if (i >= 0 && i < citiesTitle.length)
            return citiesTitle[i];
        else return city;
    }

    @Override
    public void onStart() {
        super.onStart();
//        Toast.makeText(getActivity(), "onStart " + city, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
//        Toast.makeText(getActivity(), "onResume " + city, Toast.LENGTH_SHORT).show();
    }
}
