package payasoft.prayertimes.Fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import payasoft.prayertimes.R;
import payasoft.prayertimes.models.city_by_location.CityByLocationModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrayerTimes4GPSFragment extends Fragment {
    Location loc;
    View v;
    PrayerTimesFragment prayerTimesFragment;

    public void setCity(String city) {
        this.city = city;
        ((TextView) v.findViewById(R.id.cityTitle)).setText(city);
    }

    String city;

    static PrayerTimes4GPSFragment prayerTimes4GPSFragment;

    public static PrayerTimes4GPSFragment getInstance() {
        if (prayerTimes4GPSFragment == null)
            prayerTimes4GPSFragment = new PrayerTimes4GPSFragment();
        return prayerTimes4GPSFragment;
    }

    public PrayerTimes4GPSFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_prayer_times4gps, container, false);
        prayerTimesFragment = ((PrayerTimesFragment) getChildFragmentManager().findFragmentById(R.id.gpsPrayerTimesFragment));

        // View Pager RTL support
        if (Locale.getDefault().getLanguage().equalsIgnoreCase("fa")) {
            v.setRotationY(180);
            prayerTimesFragment.setFlip(false);
        }

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1000);
        else startLocation();
        return v;
    }

    private void startLocation() {
        SmartLocation.with(getActivity()).location()
                .oneFix()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        loc = location;
                        setCityTitle(loc);
                        prayerTimesFragment.setLoc(loc);
                        prayerTimesFragment.refreshValue();
                    }
                });
    }


    private void setCityTitle(Location loc) {
        final AsyncHttpClient cityHttpClient = new AsyncHttpClient();
//        String url = String.format("http://api.aladhan.com/calendarByCity?city=%s&country=%s&month=%d&year=%d&method=7",
//                city, country, date.getYear(), date.getMonth());

        String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="
                + Double.toString(loc.getLatitude())
                + ",%20" + Double.toString(loc.getLongitude())
                + "&language=" + Locale.getDefault().getLanguage();
//        Log.i("reza", url);

        cityHttpClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                Gson gson = new Gson();
                CityByLocationModel city = gson.fromJson(response.toString(), CityByLocationModel.class);
                if (city.getResults().size() == 0)
                    setCity(getString(R.string.not_found));  // not found
                else
                    outerloop:
                            for (int i = 0; i < city.getResults().get(0).getAddressComponents().size(); i++)
                                for (int j = 0; j < city.getResults().get(0).getAddressComponents().get(i).getTypes().size(); j++) {
//                                    Log.i("type", city.getResults().get(0).getAddressComponents().get(i).getTypes().get(j).toLowerCase());
                                    if (city.getResults().get(0).getAddressComponents().get(i).getTypes().get(j).equalsIgnoreCase("locality")) {
//                                        Log.i("type", city.getResults().get(0).getAddressComponents().get(i).getLongName());
                                        setCity(city.getResults().get(0).getAddressComponents().get(i).getLongName());
                                        break outerloop;
                                    }
                                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

//                Toast.makeText(mContext, "Internet Error1 \n" + throwable, Toast.LENGTH_LONG).show();
//                showValueEvent.show();
                Toast.makeText(getActivity(), R.string.internet_error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

//                showValueEvent.show();
                Toast.makeText(getActivity(), R.string.internet_error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

//                showValueEvent.show();
                Toast.makeText(getActivity(), R.string.internet_error, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1000: {
                if ((grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) ||
                        ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    startLocation();
                } else Toast.makeText(getActivity(), "GPS can't start", Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }


}


