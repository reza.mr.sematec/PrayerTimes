package payasoft.prayertimes;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Reza on 2017/11/01.
 */

public class PrayerTimingDBHelper extends SQLiteOpenHelper {
    String db_create_query = "" +
            "CREATE TABLE timing_tbl (" +
            " _id INTEGER AUTO INCREMENT PRIMARY KEY ," +
            " city TEXT ," +
            " date TEXT ," +
            " fajr TEXT ," +
            " sunrise TEXT ," +
            " dhuhr TEXT ," +
            " asr TEXT ," +
            " sunset TEXT ," +
            " maghrib TEXT ," +
            " isha TEXT ," +
            " imsak TEXT ," +
            " midnight TEXT  " + ");" +
            "\n" +
            "CREATE TABLE city_tbl (" +
            " _id INTEGER AUTO INCREMENT PRIMARY KEY ," +
            " name TEXT ," +
            " title TEXT ," +
            " country TEXT " + ")" +
            "";

    public PrayerTimingDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(db_create_query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void inserToTiming(String city, String date, PrayerTimeItems prayerTimeItems) {
        String insertQuery = "INSERT INTO timing_tbl" +
                "(city , date, fajr, sunrise, dhuhr, asr, sunset, maghrib, isha, imsak, midnight)" +
                "VALUES( '" + city +
                "'   ,  '" + date +
                "'   ,  '" + prayerTimeItems.getFajr() +
                "'   ,  '" + prayerTimeItems.getSunrise() +
                "'   ,  '" + prayerTimeItems.getDhuhr() +
                "'   ,  '" + prayerTimeItems.getAsr() +
                "'   ,  '" + prayerTimeItems.getSunset() +
                "'   ,  '" + prayerTimeItems.getMaghrib() +
                "'   ,  '" + prayerTimeItems.getIsha() +
                "'   ,  '" + prayerTimeItems.getImsak() +
                "'   ,  '" + prayerTimeItems.getMidnight() +
                "'  )";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(insertQuery);
        db.close();
    }

    public boolean getTiming(String city, String date, PrayerTimeItems prayerTimeItems) {
        boolean res;
        SQLiteDatabase db = this.getReadableDatabase();
        String select_query = String.format("SELECT fajr" +
                ", sunrise" +
                ", dhuhr" +
                ", asr" +
                ", sunset" +
                ", maghrib" +
                ", isha" +
                ", imsak" +
                ", midnight " +
                "from timing_tbl Where city ='%s' and date='%s'", city, date);
        Cursor cursor = db.rawQuery(select_query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            prayerTimeItems.setFajr(cursor.getString(0));
            prayerTimeItems.setSunrise(cursor.getString(1));
            prayerTimeItems.setDhuhr(cursor.getString(2));
            prayerTimeItems.setAsr(cursor.getString(3));
            prayerTimeItems.setSunset(cursor.getString(4));
            prayerTimeItems.setMaghrib(cursor.getString(5));
            prayerTimeItems.setIsha(cursor.getString(6));
            prayerTimeItems.setImsak(cursor.getString(7));
            prayerTimeItems.setMidnight(cursor.getString(8));
            res = true;
        } else res = false;
        db.close();
        return res;
    }
}
