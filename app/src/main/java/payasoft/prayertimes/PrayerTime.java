package payasoft.prayertimes;

import android.content.Context;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import net.time4j.PlainDate;
import net.time4j.format.expert.ChronoFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import payasoft.prayertimes.models.timing_by_city.TimingsByCityModel;

/**
 * Created by Reza on 2017/10/30.
 */

class PrayerTimeItems {
    String fajr;
    String sunrise;
    String dhuhr;
    String asr;
    String sunset;
    String maghrib;
    String isha;
    String imsak;
    String midnight;

    public String getMidnight() {
        return midnight;
    }

    public void setMidnight(String midnight) {
        this.midnight = midnight;
    }

    public String getFajr() {
        return fajr;
    }

    public void setFajr(String fajr) {
        this.fajr = fajr;
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getDhuhr() {
        return dhuhr;
    }

    public void setDhuhr(String dhuhr) {
        this.dhuhr = dhuhr;
    }

    public String getAsr() {
        return asr;
    }

    public void setAsr(String asr) {
        this.asr = asr;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public String getMaghrib() {
        return maghrib;
    }

    public void setMaghrib(String maghrib) {
        this.maghrib = maghrib;
    }

    public String getIsha() {
        return isha;
    }

    public void setIsha(String isha) {
        this.isha = isha;
    }

    public String getImsak() {
        return imsak;
    }

    public void setImsak(String imsak) {
        this.imsak = imsak;
    }
}

public class PrayerTime {
    PrayerTimeItems prayerTimeItems;
    String city;
    PlainDate date;
    Context mContext;
    PrayerTimingDBHelper dbHandler;
    ShowValueEvent showValueEvent;
    Location location;

    // برای شهر و زمان مورد نظر
//    public PrayerTime(String city, PlainDate date, Context mContext, ShowValueEvent showValueEvent) {
//        this.city = city;
//        this.date = date;
//        this.mContext = mContext;
//        this.showValueEvent = showValueEvent;
//    }

    // برای امروز تهران
    public PrayerTime(Context mContext, String city, PrayerTimingDBHelper dbHandler, ShowValueEvent showValueEvent) {
        prayerTimeItems = new PrayerTimeItems();
        this.dbHandler = dbHandler;
        this.city = city;
        this.date = PlainDate.nowInSystemTime();
        this.mContext = mContext;
        this.showValueEvent = showValueEvent;
        this.location = null;
    }

    public PrayerTime(Context mContext, Location location, ShowValueEvent showValueEvent) {
        prayerTimeItems = new PrayerTimeItems();
        this.dbHandler = dbHandler;
        this.city = null;
        this.date = PlainDate.nowInSystemTime();
        this.mContext = mContext;
        this.showValueEvent = showValueEvent;
        this.location = location;
    }

    private void clear() {
        prayerTimeItems.setFajr("");
        prayerTimeItems.setSunrise("");
        prayerTimeItems.setDhuhr("");
        prayerTimeItems.setAsr("");
        prayerTimeItems.setSunset("");
        prayerTimeItems.setMaghrib("");
        prayerTimeItems.setIsha("");
        prayerTimeItems.setImsak("");
        prayerTimeItems.setMidnight("");
    }

    private String checkTime(String t) {
        Date dt;
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
            dt = fmt.parse(t);

            return String.format("%tR", dt);
        } catch (Exception e) {
            return "";
        }
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAPrayerTime(String name) {
        String t;

        switch (name) {
            case "Fajr":
                t = prayerTimeItems.getFajr();
                break;
            case "Sunrise":
                t = prayerTimeItems.getSunrise();
                break;
            case "Dhuhr":
                t = prayerTimeItems.getDhuhr();
                break;
            case "Asr":
                t = prayerTimeItems.getAsr();
                break;
            case "Sunset":
                t = prayerTimeItems.getSunset();
                break;
            case "Maghrib":
                t = prayerTimeItems.getMaghrib();
                break;
            case "Isha":
                t = prayerTimeItems.getIsha();
                break;
            case "Imsak":
                t = prayerTimeItems.getImsak();
                break;
            case "Midnight":
                t = prayerTimeItems.getMidnight();
                break;
            default:
                t = null;
                break;
        }
        return checkTime(t);
    }

    private String getCountry() {
        String citiesName[] = mContext.getResources().getStringArray(R.array.city_names);
        String countries[] = mContext.getResources().getStringArray(R.array.country);

        int i = Arrays.asList(citiesName).indexOf(city);
        if (i >= 0 && i < countries.length)
            return countries[i];
        else return "IR";
    }

    public void refreshFromInternet() {
        final AsyncHttpClient prayerTimeHttpClient = new AsyncHttpClient();
//        String url = String.format("http://api.aladhan.com/calendarByCity?city=%s&country=%s&month=%d&year=%d&method=7",
//                city, country, date.getYear(), date.getMonth());
        String url;
        if (city != null)
            url = String.format("http://api.aladhan.com/timingsByCity?city=%s&country=%s&method=7",
                    city, getCountry());
        else
            url = "http://api.aladhan.com/timings/0?latitude=" + Double.toString(location.getLatitude()) + "&longitude=" + Double.toString(location.getLongitude()) + "&method=7";

        prayerTimeHttpClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                Gson gson = new Gson();
                TimingsByCityModel timings = gson.fromJson(response.toString(), TimingsByCityModel.class);
                Log.i("org", timings.getData().getTimings().getFajr());
                prayerTimeItems.setFajr(timings.getData().getTimings().getFajr());
                prayerTimeItems.setSunrise(timings.getData().getTimings().getSunrise());
                prayerTimeItems.setDhuhr(timings.getData().getTimings().getDhuhr());
                prayerTimeItems.setAsr(timings.getData().getTimings().getAsr());
                prayerTimeItems.setSunset(timings.getData().getTimings().getSunset());
                prayerTimeItems.setMaghrib(timings.getData().getTimings().getMaghrib());
                prayerTimeItems.setIsha(timings.getData().getTimings().getIsha());
                prayerTimeItems.setImsak(timings.getData().getTimings().getImsak());
                prayerTimeItems.setMidnight(timings.getData().getTimings().getMidnight());
                if (city != null) // Not insert for GPS state
                    dbHandler.inserToTiming(city, getDateKey(), prayerTimeItems);
                showValueEvent.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

//                Toast.makeText(mContext, "Internet Error1 \n" + throwable, Toast.LENGTH_LONG).show();
                showValueEvent.show();
                Toast.makeText(mContext, R.string.internet_error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                showValueEvent.show();
                Toast.makeText(mContext, R.string.internet_error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                showValueEvent.show();
                Toast.makeText(mContext, R.string.internet_error, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void refresh() {
        clear();
        if (city != null) {
            if (!refreshFromDb()) {
                refreshFromInternet();
            }
        } else if (location != null) {
            refreshFromInternet();
        }
    }

    private boolean refreshFromDb() {
        boolean res = dbHandler.getTiming(city, getDateKey(), prayerTimeItems);
        if (res)
            showValueEvent.show();

        return res;
    }

    private String getDateKey() {
        ChronoFormatter f;
        f = ChronoFormatter.setUp(PlainDate.axis(), Locale.US)
                .addInteger(PlainDate.DAY_OF_MONTH, 1, 2)
                .addLiteral('/')
                .addText(PlainDate.MONTH_OF_YEAR)
                .addLiteral('/')
                .addInteger(PlainDate.YEAR, 1, 4)
                .build();
        return f.format(date);
    }
}
